import React, {useState} from 'react'
import CrudForm from './CrudForm'
import CrudTable from './CrudTable'

const initialDB = [
    {
        id: 1,
        name: "Seiya",
        constelation: "Pegaso"
    },
    {
        id: 2,
        name: "Shiriu",
        constelation: "Dragón"
    },
    {
        id: 3,
        name: "Hyoga",
        constelation: "Cisne"
    },
    {
        id: 4,
        name: "Shun",
        constelation: "Andrómeda"
    },
    {
        id: 5,
        name: "Ikki",
        constelation: "Fenix"
    }
]

const CrudApp = () => {
    const [db, setDb] = useState(initialDB)

    const [dataToEdit, setDataToEdit] = useState(null);

    const createData = data => {
        data.id = Date.now()
        console.log(data);
        setDb([
            ...db,
            data
        ])
        // setDb([
        //     ...db,
    
        // ])
    }

    const updateData = data => {
        let newData = db.map(el => el.id === data.id ? data: el)
        setDb(newData);
    }

    const deleteData = (id) => {
        let isDelete = window.confirm(`¿Estas seguro que quieres eliminar el registro don id ${id}?`)
        if(isDelete) {
            let newData = db.filter (el => el.id !== id)
            setDb(newData)
        } else {
            return;
        }
    }

    return (
        <div>
            <h2>Crud App</h2>
            <article className='grid-1-2'>
                <CrudForm
                    createData={createData}
                    updateData = {updateData}
                    dataToEdit = {dataToEdit}
                    setDataToEdit = {setDataToEdit}
                />
                <CrudTable data = {db}
                    setDataToEdit ={ setDataToEdit}
                    deleteData ={deleteData}
                />
            </article>
        </div>
    )
}

export default CrudApp
